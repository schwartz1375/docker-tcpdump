# Tcpdump Docker Image
This is a simple docker image that runs tcpdump on an Ubuntu image and writes the tcpdump output file to the volume /data.  Note in this configuration the system will keep at max 10x 1GB files and overwrite the oldest one.

## Building the Image
Build the docker image with the following command:

    $ docker build -t schwartz1375/docker-tcpdump:latest -f ./Dockerfile .

## Usage
To capture data on the hosts network interfaces, you need to run the container by using the host networking mode.  Note that there --net=host doesn't work as expected on Mac OS X ([see the Docker Forum](https://forums.docker.com/t/explain-networking-known-limitations-explain-host/15205)):

    $ docker run --net=host schwartz1375/docker-tcpdump

To specify filters or interface, you can use this image as you would use tcpdump, but this will override default parameters:

    $ docker run --net=host schwartz1375/docker-tcpdump -i eth2 port 80

If you want log the data (PCAP's) to the host, we use the docker -v command to mount the volume:

    $ docker run --net=host -v $PWD:/data schwartz1375/docker-tcpdump -i any -w /data/dump.pcap "tcp"
